package at.patrickpoechhacker.newtictactoe;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by patrickpoechhacker on 18.10.15.
 */
public class MenuActivity extends AppCompatActivity {

    public static int option;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Button buttonVsPlayer = (Button) findViewById(R.id.buttonPlayerVsPlayer);
        buttonVsPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Player vs. Player
                option = 1;
                Intent intent = new Intent(MenuActivity.this, GameActivity.class);
                startActivity(intent);
            }
        });

        Button buttonVsKI = (Button) findViewById(R.id.buttonPlayerVsKIEasy);
        buttonVsKI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Player vs. KI (Easy)
                option = 2;
                Intent intent = new Intent(MenuActivity.this, GameActivity.class);
                startActivity(intent);
            }
        });
    }

}
