package at.patrickpoechhacker.newtictactoe;

import android.util.Log;

/**
 * Created by patrickpoechhacker on 12.10.15.
 */
public class GameLogic {

    private boolean playersTurn; // Who's turn is it? true = X false = O
    public char board[][];       // Represent the board as an array of characters
    private char winner = '\0';  // Represents the winner. Character \0 = NULL in Java
    private int counter = 0;

    public GameLogic(boolean playersTurn, char[][] board) {
        this.playersTurn = playersTurn;
        this.board = board;
    }

    public boolean checkWin() {
        counter++;
        Log.d("counter", String.valueOf(counter));
        if (checkWinner(board, 3, 'X')) {
            winner = 'X';
        } else if (checkWinner(board, 3, 'O')) {
            winner = 'O';
        }

        if (winner == '\0') {
            return false;   // Kein Gewinner
        } else {
            return true;    // Gewinner gefunden
        }
    }

    private boolean checkWinner(char[][] board, int size, char player){
        //Durchläuft jede Spalte
        for(int i = 0; i < size; i++){
            int total = 0;
            for (int y = 0; y < size; y++) {
                if (board[i][y] == player) {
                    total++;
                }
            }
            if (total >= size) {
                return true; // Gewinner
            }
        }

        //Durchläuft jede Reihe
        for(int i = 0; i < size; i++){
            int total = 0;
            for (int y = 0; y < size; y++) {
                if (board[y][i] == player) {
                    total++;
                }
            }
            if (total >= size) {
                return true; // Gewinner
            }
        }

        // Vorwärts Diagonal
        int total = 0;
        for (int i = 0; i < size; i++) {
            for (int y = 0; y < size; y++) {
                if (i == y && board[i][y] == player) {
                    total++;
                }
            }
        }

        if (total >= size) {
            return true; // Gewinner
        }

        // Rückwärts Diagonal
        total = 0;
        for (int i = 0; i < size; i++) {
            for (int y = 0; y < size; y++) {
                if (i + y == size - 1 && board[i][y] == player) {
                    total++;
                }
            }
        }

        if (total >= size) {
            return true; // Gewinner
        }

        return false; // Kein Gewinner
    }


    //Getters & Setters
    public boolean isPlayersTurn() {
        return playersTurn;
    }

    public void setPlayersTurn(boolean playersTurn) {
        this.playersTurn = playersTurn;
    }

    public void setBoard(char[][] board) {
        this.board = board;
    }

    public char getWinner() {
        return winner;
    }

    public void setWinner(char winner) {
        this.winner = winner;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
}
