package at.patrickpoechhacker.newtictactoe;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;

public class GameActivity extends AppCompatActivity {

    private GameLogic game = new GameLogic(true, new char[3][3]);
    ArrayList<Button> buttonList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonList.add((Button) findViewById(R.id.button1));
        buttonList.add((Button) findViewById(R.id.button2));
        buttonList.add((Button) findViewById(R.id.button3));
        buttonList.add((Button) findViewById(R.id.button4));
        buttonList.add((Button) findViewById(R.id.button5));
        buttonList.add((Button) findViewById(R.id.button6));
        buttonList.add((Button) findViewById(R.id.button7));
        buttonList.add((Button) findViewById(R.id.button8));
        buttonList.add((Button) findViewById(R.id.button9));

        setupOnClickListeners();
        resetButtons();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("gameOver"));
        LocalBroadcastManager.getInstance(this).registerReceiver(AITurn, new IntentFilter("performClick"));
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            TextView tf = (TextView) findViewById(R.id.playersTurnTf);
            tf.setText("Game Over!");
            disableButtons();
        }
    };

    private BroadcastReceiver AITurn = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean randomSet = false;
            int randomNum = (int) (Math.random() * 9 - 1);

            do {
                if (buttonList.get(randomNum).isEnabled()) {
                    buttonList.get(randomNum).performClick();
                    randomSet = true;
                    break;
                } else {
                    randomNum = (int) (Math.random() * 9 - 1);
                    continue;
                }
            } while(randomSet == false);
        }
    };

    //Diese Funktion macht jeden Button klickbar.
    private void setupOnClickListeners() {
        TableLayout tableLayout = (TableLayout) findViewById(R.id.tableLayout);
        //Durchläuft die Tabelle nach jedem Button
        for (int y = 0; y < tableLayout.getChildCount(); y++) {
            if (tableLayout.getChildAt(y) instanceof TableRow) {
                TableRow row = (TableRow) tableLayout.getChildAt(y);
                for (int x = 0; x < row.getChildCount(); x++) {
                    View V = row.getChildAt(x);
                    V.setOnClickListener(new StartGameOnClick(this, x, y, game));
                }
            }
        }
    }

    public void newGame() {
        TextView tf = (TextView) findViewById(R.id.playersTurnTf);
        tf.setText("Player X turn");
        game.setPlayersTurn(true);
        game.setBoard(new char[3][3]);
        game.setWinner('\0');
        game.setCounter(0);

        resetButtons();
    }

    // Setzt alle Buttons auf "" und aktiviert diese erneut.
    private void resetButtons() {
        TableLayout T = (TableLayout) findViewById(R.id.tableLayout);
        for (int y = 0; y < T.getChildCount(); y++) {
            if (T.getChildAt(y) instanceof TableRow) {
                TableRow R = (TableRow) T.getChildAt(y);
                for (int x = 0; x < R.getChildCount(); x++) {
                    if (R.getChildAt(x) instanceof Button) {
                        Button B = (Button) R.getChildAt(x);
                        B.setText("");
                        B.setEnabled(true);
                    }
                }
            }
        }
    }

    //Deaktiviert alle Buttons
    private void disableButtons() {
        TableLayout T = (TableLayout) findViewById(R.id.tableLayout);
        for (int y = 0; y < T.getChildCount(); y++) {
            if (T.getChildAt(y) instanceof TableRow) {
                TableRow R = (TableRow) T.getChildAt(y);
                for (int x = 0; x < R.getChildCount(); x++) {
                    if (R.getChildAt(x) instanceof Button) {
                        Button B = (Button) R.getChildAt(x);
                        B.setEnabled(false);
                    }
                }
            }
        }
    }

    //Funktion um die ActionBar klickbar zu machen.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.startNewGame:
                Log.d(getClass().getSimpleName(), "Start new Game");
                newGame();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(AITurn);
    }
}

