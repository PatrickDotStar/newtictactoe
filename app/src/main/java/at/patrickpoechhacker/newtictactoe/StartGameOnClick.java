package at.patrickpoechhacker.newtictactoe;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by patrickpoechhacker on 12.10.15.
 */
public class StartGameOnClick implements View.OnClickListener {

    private int x = 0;
    private int y = 0;
    private GameLogic game;
    public Activity activity;

    public StartGameOnClick(Activity activity, int x, int y, GameLogic game) {
        this.activity = activity;
        this.x = x;
        this.y = y;
        this.game = game;
    }

    //Sendet eine Nachricht an die GameActivity um zu informieren, dass ein Gewinner gefunden wurde
    private void sendGameOverMessage() {
        Intent intent = new Intent("gameOver");
        LocalBroadcastManager.getInstance(null).sendBroadcast(intent);
    }

    private void performClickMessage(){
        Intent intent = new Intent("performClick");
        LocalBroadcastManager.getInstance(null).sendBroadcast(intent);
    }

    @Override
    public void onClick(View view) {
        if (view instanceof Button) {
            Button button = (Button) view;
            TextView tf = (TextView)this.activity.findViewById(R.id.playersTurnTf);

            //Player vs. Player
            if (MenuActivity.option == 1) {
                if (game.isPlayersTurn()) {
                    game.board[x][y] = 'X';
                    button.setText("X");
                    tf.setText("Player O turn");
                } else {
                    game.board[x][y] = 'O';
                    button.setText("O");
                    tf.setText("Player X turn");
                }

                button.setEnabled(false);
                game.setPlayersTurn(!game.isPlayersTurn());

                // check if anyone has won
                if (game.checkWin()) {
                    sendGameOverMessage();
                    showWinnerAlert();
                    return;
                } else if(game.getCounter() == 9) {
                    sendGameOverMessage();
                    showDrawAlert();
                    return;
                }

                //Player vs KI (Easy)
            } else if(MenuActivity.option == 2) {
                if (game.isPlayersTurn()) {
                    game.board[x][y] = 'X';
                    button.setText("X");
                    tf.setText("Player O turn");
                    button.setEnabled(false);

                    if (game.checkWin()) {
                        sendGameOverMessage();
                        showWinnerAlert();
                        return;
                    } else if (game.getCounter() == 9) {
                        sendGameOverMessage();
                        showDrawAlert();
                        return;
                    }

                    game.setPlayersTurn(!game.isPlayersTurn());
                    performClickMessage();
                } else {
                    game.board[x][y] = 'O';
                    button.setText("O");
                    tf.setText("Player X turn");
                    button.setEnabled(false);

                    game.setPlayersTurn(!game.isPlayersTurn());

                    if (game.checkWin()) {
                        sendGameOverMessage();
                        showWinnerAlert();
                        return;
                    } else if (game.getCounter() == 9) {
                        sendGameOverMessage();
                        showDrawAlert();
                        return;
                    }
                }
            }
        }
    }

    private void showWinnerAlert(){
        new AlertDialog.Builder(this.activity)
                .setTitle(game.getWinner() + " won!")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })
                .show();
    }

    private void showDrawAlert(){
        new AlertDialog.Builder(this.activity)
                .setTitle("Unentschieden!")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })
                .show();
    }
}
